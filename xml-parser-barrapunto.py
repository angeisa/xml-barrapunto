from xml.sax.handler import ContentHandler
from xml.sax import make_parser
import sys
import string
from urllib import request

class CounterHandler(ContentHandler):

    def __init__ (self):
        self.inItem = 0
        self.inContent = 0
        self.theContent = ""
        self.theTitle = ""

    def startElement (self, name, attrs):
        if name == 'item':
            self.inItem = 1
        elif name == 'title':
            if self.inItem == 1:
                self.inContent = 1
        elif name == 'link':
            if self.inItem == 1:
                self.inContent = 1

    def endElement (self, name):
        if name == 'item':
            self.inItem = 0
        if self.inItem == 1:
            if name == 'title':
                self.theTitle = self.theContent
                self.inContent = 0
                self.theContent = ""
            if name == 'link':
                archivo.write('<p>Link: ' + '<a href="' + self.theContent + '">' + self.theTitle + '</a></p>')
                self.inContent = 0
                self.theContent = ""

    def characters (self, chars):
        if self.inContent:
            self.theContent = self.theContent + chars


# Load parser and driver

archivo = open('barrapunto.html', 'w')

BarrapuntoParser = make_parser()
BarrapuntoHandler = CounterHandler()
BarrapuntoParser.setContentHandler(BarrapuntoHandler)

# Ready, set, go!

archivo.write("<html><body>")
xmlFile = request.urlopen("http://barrapunto.com/index.rss")
BarrapuntoParser.parse(xmlFile)

archivo.write("</body></html>")

print ("Parse complete")
